# -*- coding: utf-8 -*-
"""
secondOrderSystemV02:
  Beispielimplementierung für einen geschlossenen Regelkreis
  Dieses Beispiel enthält die folgenden drei Klassen
  - SecondOrderSystem:
      Implementierung eines Streckenglieds (Regelstrecke) zweiter Ordnung
      (PT2-Glied) als rekursives Filter mit den Design-Konstanten
         K, T1, T2
  - PIDControlBlock:
      Implementierung eines PID-Glieds (Regler)
      als rekursives Filter mit den Design-Konstanten
         Kp, Ki, Kd
  . ClosedLoopSystem:
      Implementierung eines geschlossenen Regelkreises, bestehend aus
        * Regler (PIDControlBLock)
        * Regelstrecke (SecondOrderSystem)

Created on Wed Mar 15 15:46:52 2023

# https://www.csestack.org/control-systems-simulation-python-example/
@author: birkudo
"""
# %% imports
import math
import numpy as np
import matplotlib.pyplot as plt

# %% filters: PT2-Glied
# PT2: recursive second order low-pass filter (PT2-Glied)
# Syntax: generate a PT2-plant with given constants K, T1, T2:
#     plant = SecondOrderSystem(K=1, T1=250, T2=100)
class SecondOrderSystem:   # PT2-Glied
    def __init__(self, K, T1, T2):
        if T1 > 0:
          e1 = -1.0 / T1
          x1 = math.exp(e1)
        else: x1 = 0
        if T2 > 0:
          e2 = -1.0 / T2
          x2 = math.exp(e2)
        else: x2 = 0
        a = 1.0 - x1    # b = x1
        c = 1.0 - x2    # d = x2
        self.K  = K
        self.ac = a * c
        self.bpd = x1 + x2
        self.bd = x1 * x2
        self.Yppr = 0
        self.Ypr = 0
 
    def __call__(self, X): 
        Y = self.ac * X + self.bpd * self.Ypr - self.bd * self.Yppr
        self.Yppr = self.Ypr
        self.Ypr = Y
        return Y*self.K

# %% PID controller (with noise filter for D-Part)
# Syntax: generate a Controller with given constants Kp, Ki, Kd:
#     controller = PIDControlBlock(Kp=1, Ki=0.1, Kd=100)
class PIDControlBlock:
  def __init__(self, Kp, Ki, Kd):
    self.Kp = Kp
    self.Ki = Ki
    self.Kd = Kd
    self.Epr = 0
    self.Eppr = 0
    self.Epppr = 0
    self.Sum = 0
 
  def __call__(self, E): 
    self.Sum += 0.5 * self.Ki * (E + self.Epr)      # where T ~1
    U = self.Kp * E + self.Sum + 0.1667 * self.Kd * (E - self.Epppr + 3.0 * (self.Epr - self.Eppr))
    self.Epppr = self.Eppr
    self.Eppr = self.Epr
    self.Epr = E
    return U

# %% combine controller + plant to a system:
class ClosedLoopSystem:
  def __init__(self, controller, plant) :
    self.C = controller
    self.P = plant
    self.Ypr = 0
 
  def __call__(self, X):
    E = X - self.Ypr
    U = self.C(E)
    Y = self.P(U)
    self.Ypr = Y
    return Y

# %% demo
# Create a plant only system (un-controlled system)
def SecondOrderDemo(K=1, T1=250, T2=100):
    Plant = SecondOrderSystem(K, T1, T2)     

    t = np.arange(1,1001)
    y = []
    for i in t:
        y.append(Plant(1))
    fig, ax = plt.subplots()
    ax.plot(t,y)
    ax.set(title='SecondOrderDemo',xlabel='time')
    plt.show()

# Create a closed loop system (controlled system)
def ClosedLoopDemo():
    Plant = SecondOrderSystem(1, 250, 100)     
    Pid   = PIDControlBlock(5, 0.0143, 356.25)
    CLS   = ClosedLoopSystem(Pid, Plant)
 
    t = np.arange(1,1001)
    y = []
    for i in t:
        y.append(CLS(1))
    fig, ax = plt.subplots()
    ax.plot(t,y)
    ax.set(title='ClosedLoopDemo',xlabel='time')
    plt.show()


# %% PIDexercise:
# Variation des geschlossenen Regelkreis-Demos "ClosedLoopDemo"
# Erzeugen eines geschlossenen Regelkreises, bestehend aus 
#    - PT2-Regelstrecke (SecondOrderSystem) mit den fixen Konstanten
#         K=1, T1=25, T2=40
#    - PID-Regler mit den einstellbaren Werten
#         Kp, Ki, Kd
#      (Voreinstellung: Kp=1, Ki=0, Kd=0)
def PIDexercise(Kp=1,Ki=0,Kd=0):
    Plant = SecondOrderSystem(K=1, T1=25, T2=40)
    Pid   = PIDControlBlock(Kp,Ki,Kd)
    CLS   = ClosedLoopSystem(Pid, Plant)
    
    # Als Referenz wird zusätzlich das Verhalten der ungeregelten 
    # Regelstrecke angezeigt. Dafür wird ein zweites PT2-Glied mit 
    # den selben Eigenschaten erzeugt:
    P2    = SecondOrderSystem(K=1, T1=25, T2=40)
 
    t = np.arange(1,501)
    y = []
    p = []
    for i in t:
        p.append(P2(1))
        y.append(CLS(1))
        
    fig = plt.figure(num='PIDexercise')  # create or get figure
    plt.figure(fig)                      # activate figure
    fig.clf()                            # clear figure (remove axes)
    ax = fig.gca()                       # create axis
    ax.plot(t,y,label='controlled plant')
    ax.plot(t,p,label='plant only')
    ax.set(title='PIDexercise',xlabel='time')
    plt.legend()
    plt.show()
    # cfm = plt.get_current_fig_manager()
    # cfm.window.activateWindow()
    # cfm.window.raise_()
    
# %%
if __name__ == "__main__":
    SecondOrderDemo()
    ClosedLoopDemo()
