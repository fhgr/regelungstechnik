# -*- coding: utf-8 -*-
"""
raumheizung_GUIv01.py

Created on Fri Feb 17 12:06:02 2023

Display a graphical user interface to model a building heating system
The building is equipped with 
  - a boiler (in the basement)
  - a room (in the ground floor) with
  - a temperature sensor
  - a radiator (heated with hot water from the basement)
  
The building is surrounded by cold air.

You may adjust: 
    - the outside temperature
    - the boiler temperature
    - the start (inside) temperature
    - the target (inside) temperature (when using a controller)

The heating may be controlled with a suitable controller (Regler)
  - the input for the controller is given by the current inside temperature

@author: birkudo
"""

from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QApplication, QDoubleSpinBox, QAbstractSpinBox, QPushButton, QComboBox
from PyQt5.QtGui import QPixmap, QPainter, QDoubleValidator, QImage
from PyQt5.QtCore import Qt, QPoint, QSize
from raumheizung_v02 import PID_Regler, Raumheizung, outputResults, closeResults, dump_to_file
import matplotlib.pyplot as plt
import numpy as np

# %% 
colors = {
    'green' : 'rgb(146,208,80)',
    'orange': 'rgb(255, 192, 0)',
    'purple': 'rgb(218,193,255)'
    }
COL_CONTROLLER = colors['orange']
COL_PLANT      = colors['green']
COL_RUN        = colors['purple']

# %%
class Label(QWidget):
    def __init__(self, parent=None, imgFilename=None):
        QWidget.__init__(self, parent=parent)
        if imgFilename is None:
            self.p = QPixmap()
        else:
            self.p = QPixmap(imgFilename)

    def setPixmap(self, p):
        self.p = p
        self.update()

    def paintEvent(self, event):
        if not self.p.isNull():
            painter = QPainter(self)
            painter.setRenderHint(QPainter.SmoothPixmapTransform)
            painter.drawPixmap(self.rect(), self.p)


class MainPixWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        lay = QVBoxLayout(self)
        lb = Label(self, "raumheizung.png")
        # lb.setPixmap(QPixmap("car.jpg"))
        lay.addWidget(lb)

# class LineWidget(QLineEdit):
#     validator = QDoubleValidator(-50.0, 150.0, 2);
#     def __init__(self, parent=None, text='', x = 100, y = 100, w = 40, h = 20):
#         QLineEdit.__init__(self, parent=parent)
#         # self.setStyleSheet("background-color: transparent; border: 0px ;")
#         self.setStyleSheet("background-color: yellow; border: 0px ;")
#         # self.setStyleSheet("background-color: rgb(255, 164, 164); border: 0px ;")
#         self.setAlignment(Qt.AlignRight)
#         pos=QPoint(x,y)
#         size=QSize(w,h)
#         self.resize(size)
#         self.move(pos)
#         self.setText(text)
#         self.setValidator(self.validator)
        
class ExtendedLineEdit(QLineEdit):
    def __init__(self, parent=None):
        QLineEdit.__init__(self, parent=parent)
        self.parent = parent
        self.setAlignment(Qt.AlignRight)
    def mouseDoubleClickEvent(self,event):
        if (event.button() == Qt.LeftButton):
            self.parent.mouseDoubleClickEvent(event);
            event.accept();
            return;
        super.mouseDoubleClickEvent(event);
    
class EditWidget(QDoubleSpinBox):
    def __init__(self, parent=None, text='', x = 100, y = 100, w = 40, h = 20, minimum = -50, maximum = 150):
        QDoubleSpinBox.__init__(self, parent=parent)
        self.parent = parent
        self.setStyleSheet("background-color: transparent; border: 0px ;")
        pos=QPoint(x,y)
        size=QSize(w,h)
        self.resize(size)
        self.move(pos)
        self.setMinimum(float(minimum))
        self.setMaximum(float(maximum))
        self.setDecimals(2)
        self.setSingleStep(0.1)
        self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.le = ExtendedLineEdit(self)
        self.setLineEdit(self.le)
        self.setValue(float(text))
        # self.editingFinished.connect(parent.on_edit_done)
    def mouseDoubleClickEvent(self, evt):
        self.parent.on_edit_double_click()

class RaumheizungWindow(QMainWindow):
    def __init__(self, windowsize = None):
        super().__init__()
        self.setWindowTitle('Raumheizung UB')
        self.setStyleSheet("background-color: white;")
        if windowsize is None:
            windowsize = QSize(640, 480)
        self.windowsize = windowsize
        self.initUI()

    def initUI(self):
        self.setFixedSize(self.windowsize)
        # self.resize(self.windowsize)
        # self.setWindowFlags(Qt.CustomizeWindowHint | Qt.FramelessWindowHint)
        setattr(self, 'showAdvancedOptions', False)

        widget = QWidget()
        setattr(widget, 'on_edit_done', self.on_edit_done)
        setattr(widget, 'on_edit_double_click', self.on_edit_double_click)
        self.setCentralWidget(widget)
        pixmap1 = QPixmap('raumheizung.png')
        pixmap1 = pixmap1.scaledToWidth(self.windowsize.width())
        self.image = QLabel()
        self.image.setPixmap(pixmap1)

        layout_box = QHBoxLayout(widget)
        layout_box.setContentsMargins(0, 0, 0, 0)
        # layout_box.addWidget(self.image)
        layout_box.addWidget(MainPixWidget(self))

        self.pixmapOpen = QPixmap('windowOpen.png')
        self.pixmapClosed = QPixmap('windowClosed.png')
        blank = np.zeros((100,100,3),dtype=np.uint8)+255
        height, width, channel = blank.shape
        bytesPerLine = 3 * width
        qImg = QImage(blank.data, width, height, bytesPerLine, QImage.Format_RGB888)
        self.pixmapWhite = QPixmap(qImg)  # ('whiteOut.png')

        self.imageRegler = Label(widget)
        self.imageRegler.setPixmap(self.pixmapWhite)
        self.imageRegler.setFixedSize(QSize(119,62))
        self.imageReglerOffset = QPoint(120,312)
        p = self.geometry().bottomRight() - self.imageRegler.geometry().bottomRight() - self.imageReglerOffset
        self.imageRegler.move(p)

        self.imageWindow = Label(widget)
        self.imageWindow.setPixmap(self.pixmapClosed)
        # self.imageWindow.setFixedSize(pixmap2.size())
        self.imageWindow.setFixedSize(QSize(120,80))
        self.imageWindowOffset = QPoint(220,290)
        p = self.geometry().bottomRight() - self.imageWindow.geometry().bottomRight() - self.imageWindowOffset
        self.imageWindow.move(p)
        self.imageWindow.__setattr__('open', False)
        self.imageWindow.__setattr__('mouseDoubleClickEvent', self.windowToggle)
        # .connect(self.windowToggle)

        self.imageVerlust = Label(widget)
        self.imageVerlust.setPixmap(self.pixmapWhite)
        self.imageVerlust.setFixedSize(QSize(120,30))
        imageVerlustOffset = QPoint(300,217)
        p = self.geometry().bottomRight() - self.imageVerlust.geometry().bottomRight() - imageVerlustOffset
        self.imageVerlust.move(p)

        self.imageLaufzeit = Label(widget)
        self.imageLaufzeit.setPixmap(self.pixmapWhite)
        self.imageLaufzeit.setFixedSize(QSize(140,30))
        imageLaufzeitOffset = QPoint(265,168)
        p = self.geometry().bottomRight() - self.imageLaufzeit.geometry().bottomRight() - imageLaufzeitOffset
        self.imageLaufzeit.move(p)

        
        self.T_target = EditWidget(widget, '20.0', 427, 137)
        self.T_start  = EditWidget(widget, '10.0', 432, 185)
        self.T_aussen = EditWidget(widget, '-10.0', 542, 200)
        self.Delta_T  = EditWidget(widget, '10.0', 275, 242)
        self.totzeit  = EditWidget(widget, '2.5', 300, 284)
        self.T_vorlauf= EditWidget(widget, '50.0', 263, 338)
        self.T_target.hide()
        self.Delta_T.hide()
        self.totzeit.hide()
    
        buttonRun = QPushButton(widget)
        # buttonRun.setStyleSheet("background-color: rgb(146, 208, 80);")
        buttonRun.setStyleSheet('QPushButton {background-color: '+f'{COL_RUN};'+' color: black;}')
        # buttonRun.setStyleSheet("background-color: rgb(146, 208, 80);")
        buttonRun.setText("run")
        buttonRun.setToolTip('Simulation ausführen')
        buttonRun.resize(50,20)
        buttonRun.move(550,20) # to move the button
        buttonRun.clicked.connect(self.run)
        
        buttonSave = QPushButton(widget)
        buttonSave.setStyleSheet('QPushButton {background-color: '+f'{COL_RUN};'+' color: black;}')
        # buttonRun.setStyleSheet("background-color: rgb(146, 208, 80);")
        buttonSave.setText("to csv")
        buttonSave.setToolTip('Letzte Simulationsdaten exportieren (simulated_data.csv)')
        buttonSave.resize(50,20)
        buttonSave.move(550,45) # to move the button
        buttonSave.clicked.connect(self.save_results)
        
        self.controllerSelect = QComboBox(widget)
        self.controllerSelect.addItem("- none -")
        self.controllerSelect.addItem("P-Regler")
        self.controllerSelect.addItem("PI-Regler")
        self.controllerSelect.addItem("PID-Regler")
        self.controllerSelect.setStyleSheet("background-color: "+f"{COL_CONTROLLER};")
        self.controllerSelect.resize(150,20)
        self.controllerSelect.move(10,40) # to move the button
        self.controllerSelect.activated[str].connect(self.onControllerChanged)
        
        self.label_Apkt = QLabel(widget)
        self.label_Apkt.setStyleSheet(f"background-color: {COL_CONTROLLER};")
        self.label_Apkt.setText(' Arbeitspunkt')
        self.label_Apkt.move(70, 70)
        self.label_Apkt.resize(85,20)
        self.edit_Apkt = EditWidget(widget, '0.0', 10, 70, w=60)
        self.edit_Apkt.setStyleSheet(f"background-color: {COL_CONTROLLER}; border: 1px ;")

        self.label_Kp = QLabel(widget)
        self.label_Kp.setStyleSheet(f"background-color: {COL_CONTROLLER};")
        self.label_Kp.setText(' Kp')
        self.label_Kp.move(70, 95)
        self.label_Kp.resize(85,20)
        self.edit_Kp = EditWidget(widget, '1.0', 10, 95, w=60)
        self.edit_Kp.setStyleSheet(f"background-color: {COL_CONTROLLER}; border: 1px ;")

        self.label_Ki = QLabel(widget)
        self.label_Ki.setStyleSheet(f"background-color: {COL_CONTROLLER};")
        self.label_Ki.setText(' Ki')
        self.label_Ki.move(70, 120)
        self.label_Ki.resize(85,20)
        self.edit_Ki = EditWidget(widget, '1.0', 10, 120, w=60)
        self.edit_Ki.setStyleSheet(f"background-color: {COL_CONTROLLER}; border: 1px ;")

        self.label_Kd = QLabel(widget)
        self.label_Kd.setStyleSheet(f"background-color: {COL_CONTROLLER};")
        self.label_Kd.setText(' Kd')
        self.label_Kd.move(70, 145)
        self.label_Kd.resize(85,20)
        self.edit_Kd = EditWidget(widget, '1.0', 10, 145, w=60)
        self.edit_Kd.setStyleSheet(f"background-color: {COL_CONTROLLER}; border: 1px ;")
        
        self.onControllerChanged('- none -')
        
    def windowToggle(self, event):
        isOpened = self.imageWindow.open
        if isOpened:
            self.imageWindow.setPixmap(self.pixmapClosed)
        else:
            self.imageWindow.setPixmap(self.pixmapOpen)
        # self.imageWindow.setFixedSize(pixmap2.size())
        # self.imageWindow.setFixedSize(QSize(120,80))
        self.imageWindow.open = not isOpened


    def toggleAdvancedOptions(self, enable=None):
        if enable is None:
            enable = not self.showAdvancedOptions
        
        for elmt in [self.Delta_T, self.totzeit]:
            if enable:
                elmt.show()
            else:
                elmt.hide()
        for elmt in [self.imageLaufzeit, self.imageVerlust]:
            if enable:
                elmt.hide()
            else:
                elmt.show()
        self.showAdvancedOptions = enable
        
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            self.test_method()
        elif event.key() in [Qt.Key_Escape]:
            self.closeFigures(closeMainWin=True)
        elif event.key() == Qt.Key_F8:
            self.toggleAdvancedOptions()

    def test_method(self):
        print('Space key pressed')


    def onControllerChanged(self, text):
        if text == "- none -":
            self.T_target.hide()
            self.imageRegler.show()
            bShow = [False, False, False, False]
        else:
            self.T_target.show()
            self.imageRegler.hide()
            if text == "P-Regler":
                bShow = [True, True, False, False]
            elif text == "PI-Regler":
                bShow = [True, True, True, False]
            else:
                bShow = [True, True, True, True]

        print('Regler: ', text)
        labels = [self.label_Apkt, self.label_Kp, self.label_Ki, self.label_Kd]
        edits = [self.edit_Apkt, self.edit_Kp, self.edit_Ki, self.edit_Kd]
        for b,l,e in zip(bShow,labels,edits):
            if b:
                l.show()
                e.show()
            else:
                l.hide()
                e.hide()
        
    def on_edit_done(self):
        self.run()
        
    def on_edit_double_click(self):
        print('doubleClick')
        
    def save_results(self):
        global heizung, regler
        dump_to_file(heizung, regler)
        
    def run(self):
        global heizung, regler
        T_target = self.T_target.value()
        T_start = self.T_start.value()
        T_aussen = self.T_aussen.value()
        Delta_T = self.Delta_T.value()
        totzeit = self.totzeit.value()
        vorlauf_temperatur = self.T_vorlauf.value()
        if self.imageWindow.open:
            tau_Auskühl = 10
        else:
            tau_Auskühl = 40
        print(vorlauf_temperatur)
        params = {'theta_i': T_start,
                  'theta_a': T_aussen,
                  'Delta_verlust': Delta_T,
                  'tau_Auskühl': tau_Auskühl,
                  'tau_Totzeit': totzeit}
        heizung = Raumheizung(params)
        text = text = str(self.controllerSelect.currentText())
        if text == '- none -':
            regler  = None  # ungeregelter Heizkreislauf, gibt immer 50 zurück
        else:
            Arbeitspunkt = self.edit_Apkt.value()
            Kp = self.edit_Kp.value()
            Ki = self.edit_Ki.value()
            Kd = self.edit_Kd.value()

            if text == 'P-Regler':
                # regler  = PID_Regler(Arbeitspunkt = Arbeitspunkt)  # ungeregelter Heizkreislauf, gibt immer Arbeitspunkt zurück
                regler  = PID_Regler(set_point = T_target, Kp = Kp, Arbeitspunkt = Arbeitspunkt)  # P-Regler mit Arbeitspunkt
            elif text == 'PI-Regler':
                regler  = PID_Regler(set_point = T_target, Kp = Kp, Ki = Ki, Arbeitspunkt = Arbeitspunkt)  # PI-Regler mit Arbeitspunkt 
            elif text == 'PID-Regler':
                regler  = PID_Regler(set_point = T_target, Kp = Kp, Ki = Ki, Kd = Kd, Arbeitspunkt = Arbeitspunkt)  # PI-Regler mit Arbeitspunkt 
            else:
                raise(Exception("Error: unknown controller type"))
        
        simulationsdauer = 60 # Dauer in Minuten
        clip_simulation = 0   # start index for plot output
        simulation_type = ''  # default simulation type
    
        while heizung.t < simulationsdauer:
            T_current = heizung.step(vorlauf_temperatur)
            if regler is not None:
                vorlauf_temperatur = regler.step(T_current)
                
        outputResults(heizung, regler, clip_simulation, simulation_type)
                
    def dummy(self):
        pass
    
    def closeEvent(self,event):
        self.closeFigures(closeMainWin=False)
        
    def closeFigures(self, closeMainWin=True):
        setattr(self, 'run', self.dummy)
        if(closeMainWin):
            self.close()
        QApplication.quit()
        closeResults()

if __name__ == '__main__':
    import sys
    import os
    global heizung, regler
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance() 
    screensize = app.desktop().availableGeometry().size()

    # ex = ExampleWindow(screensize)
    ex = RaumheizungWindow()
    ex.show()
    ex.run()

    try:
        if os.environ.get('SPY_UMR_ENABLED'):
            app.exec_()
        else:
            sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')
