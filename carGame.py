# -*- coding: utf-8 -*-
"""
Module carGame.py
Created on Mon Jan 30 08:11:20 2023
@author: birkudo

Instructions:
=============
    Click and hold mouse to start the game.
    Drag mouse left/right for steering.

"""
import numpy as np
from numpy import sin, cos, pi
import cv2 as cv
import time
import matplotlib.pyplot as plt

π = pi
PARCOURS_FILE = 'parcours01.png'

# %%
DELAY = 25.0  # delay in ms before steering takes place

# %%
class vehicle():
    pos = np.array([225.,200.]).astype(float)
    angle = π*180/180
    speed = 2.
    h,w = 10,5
    color = (0, 255, 255);
    isMoving = False

    def __init__(self, game = None):
        self.game = game
        self.restart()
    
    def restart(self):
        self.pos = np.array([225.,200.]).astype(float)
        self.angle = π*180/180
        self.speed = 2.
        self.isMoving = False
        
    def setMovement(self, isMoving):
        self.isMoving = isMoving
        
    def step(self, img, dAngle=0.0):
        # cv::circle (InputOutputArray img, Point center, int radius, const Scalar &color, int thickness=1, int lineType=LINE_8, int shift=0)
        if self.isMoving:
            self.angle += dAngle
            self.pos = self.pos + np.array([sin(self.angle), cos(self.angle)])*self.speed
        mask = cv.circle(np.zeros_like(img), self.pos.astype(int), self.w, (255,255,255), -1, cv.LINE_AA)
        collision = np.any(np.bitwise_and(img>0,mask>0))
        img = cv.circle(img, self.pos.astype(int), self.w, self.color, -1, cv.LINE_AA)
        img = cv.putText(
            img, "highscore", (20,300), cv.FONT_HERSHEY_SIMPLEX, 
            0.8, (0, 80, 209, 255), 2) #font stroke
        img = cv.putText(
            img, f"{int(self.game.highscore):7d}", (20,330), cv.FONT_HERSHEY_SIMPLEX, 
            0.8, (0, 80, 209, 255), 2) #font stroke
        if self.isMoving:
            img = cv.putText(
                img, "score", (40,400), cv.FONT_HERSHEY_SIMPLEX, 
                0.8, (209, 80, 0, 255), 2) #font stroke
            img = cv.putText(
                img, f"{int(self.game.score):7d}", (20,430), cv.FONT_HERSHEY_SIMPLEX, 
                0.8, (209, 80, 0, 255), 2) #font stroke
        return img, collision
    
    def drawRect(self, img):
        # Create the rotated rectangle
        pos = self.pos.astype(int)
        rotatedRectangle = cv.RotatedRect((pos[0], pos[1]), (self.h, self.w), self.angle);
    
        box = cv.boxPoints(rotatedRectangle)
        box = np.intp(box) #np.intp: Integer used for indexing (same as C ssize_t; normally either int32 or int64)
        img = cv.drawContours(img, [box], 0, self.color)
        return img
    
# %%
class carGame():
    field = np.zeros((480,640,3))
    WND_NAME = "carGame | press 'q' to quit."
    FPS = 60.
    delay = float(DELAY) # delay in ms
    
    numActions = np.maximum(1,int(delay / (1000./FPS) ))
    actions = np.zeros((numActions,))
    maxAngle = 30.
    deltaAngle = 2.
    steeringAngleDeg = 0.0
    LBUTTONDOWN = False
    mouse_x0 = 0
    raceStart = 0
    score = 0
    highscore = 0
    
    def __init__(self):
        self.vehicle = vehicle(self)
        self.createParcours()
        cv.namedWindow(self.WND_NAME, cv.WINDOW_NORMAL)
        cv.setMouseCallback(self.WND_NAME, self.onMouse)
        
    def createParcours(self):
        bkg = 255-cv.imread(PARCOURS_FILE,0)
        ret, bw = cv.threshold(bkg,1,255,cv.THRESH_BINARY)
        bw[:5,:]=0; bw[-5:,:]=0; bw[:,:5]=0; bw[:,-5:]=0
        bw = cv.resize(bw, (480,480))
        contours, hierarchy = cv.findContours(bw, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        assert(len(contours) == 1)
        # cnt = contours[0]
        # cnt[:,0,0] += 160
        # bw = cv.cvtColor(bw, cv.COLOR_GRAY2BGR)
        bw = cv.drawContours(bw, contours, 0, 255, 50)
        self.field[:,160:,1] = 255-bw
        
        
    # def steerRight(self):
    #     self.steeringAngleDeg = np.maximum(-self.maxAngle,self.steeringAngleDeg-self.deltaAngle)
    
    # def steerLeft(self):
    #     self.steeringAngleDeg = np.minimum(self.maxAngle,self.steeringAngleDeg+self.deltaAngle)
    
    def getAngle(self):
       return π*self.steeringAngleDeg/180
        
    def drawSteeringWheel(self, img):
        center = (80,80)
        color = (0,102,153)
        toff = pi/3
        r = 25
        img = cv.circle(img, center, 2*r, color, 10)
        theta = 4.0*self.getAngle()
        pt2a = np.array((center[0]+1.8*r*sin(theta-toff), center[1]+1.8*r*cos(theta-toff))).astype(int)
        pt2b = np.array((center[0]+1.8*r*sin(theta+toff), center[1]+1.8*r*cos(theta+toff))).astype(int)
        img = cv.line(img, center, tuple(pt2a), color, 10)
        img = cv.line(img, center, tuple(pt2b), color, 10)
        return img
    
    def onMouse(self, event, x, y, flags, param):
        # check if left mouse button was clicked
        if event == cv.EVENT_LBUTTONDOWN:
            self.LBUTTONDOWN = True
            self.mouse_x0 = x
            self.vehicle.setMovement(True)
            self.raceStart = time.time()
              
        # check if left mouse button was released
        if event == cv.EVENT_LBUTTONUP:
            self.LBUTTONDOWN = False
            self.steeringAngleDeg = 0.0
            
        if event == cv.EVENT_MOUSEMOVE:
            if self.LBUTTONDOWN:
                self.setMouseAngle(x)
    
    def setMouseAngle(self, x):
        dX = self.mouse_x0 - x
        if dX >= 0:
            angleDeg = np.minimum(float(dX)/10,self.maxAngle)
        else:
            angleDeg = np.maximum(float(dX)/10,-self.maxAngle)
        self.steeringAngleDeg = angleDeg
        
    def run(self):
        isRunning = True
        loopTime = 1000./self.FPS
        lastTime = time.time()
        while isRunning:
            img = self.field.copy()
            img, isCollision = self.vehicle.step(img, self.actions[-1])
            if isCollision:
                self.vehicle.restart()
                self.vehicle.setMovement(False)
                self.highscore = np.maximum(self.highscore, self.score)
            img = self.drawSteeringWheel(img)
            cv.imshow(self.WND_NAME, img)
            curTime = time.time()
            self.score = np.round(1000*(curTime-self.raceStart))
            waitTime = loopTime - (curTime-lastTime)
            # print(waitTime)
            lastTime = curTime
            ch = cv.waitKey(np.maximum(1,int(waitTime)))
            self.actions[-1] = 0.2*self.getAngle()
            self.actions = np.roll(self.actions,-1)
            if ch == ord('q'):
                break
            if ch == 27:
                break
        cv.destroyWindow(self.WND_NAME)
        
if __name__=="__main__":
    print(__doc__)
    game = carGame()
    game.run()
    