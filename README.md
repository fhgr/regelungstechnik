# Regelungstechnik (REGTEC)

Tools and snippets for the lecture series Regelungstechnik.

## Installation:

Manual installation: 
Press the download-button above, to download a zipped version of all files in this repository.

Optional installation using git:
git clone https://gitlab.com/fhgr/regelungstechnik


## Contents:

_________________

**[carGame.py](./carGame.py)**  
Display an interactive game to experience the dead time (deutsch: Totzeit) of a system.  
Click and hold down mouse button to play. Drag left/right for steering.  
![carGame screenshot](./carGame_screenshot.png "carGame_screenshot")

_________________

**[raumheizung_GUIv01.py](./raumheizung_GUIv01.py)**  
Display a GUI to simulate a heating system and its control.
![raumheizung screenshot](./raumheizung_screenshot.png "raumheizung_screenshot")

