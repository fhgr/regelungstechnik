# -*- coding: utf-8 -*-
"""
Created on Wed Feb  27 10:05:11 2023

https://www.youtube.com/watch?v=quqxyny5kBU

- Generelle Simulation einer Raumheizung
- Modellbildung und Euler-Integration (numerische Lösung der Differentialgleichungen)
    * Aufwärmverhalten des Raums (Heizkörper-Temperatur > Raumtemp.)
    * Zeitverhalten (Heizung im Keller -> Totzeit bis am Heizkörper ankommt)
    * Abkühlverhalten durch Wärmeabgabe (Aussenwand)
- Simulation des Temperaturverhaltens ohne und mit Regeleinrichtung (Regler)
- Implementierung verschiedener Regler-Typen (P-Regler, PI-Regler, etc. mit/ohne Arbeitspunkt)

Drei Klassen:
    - Raumheizung: numerische Simulation des Systems
    - PID_Regler:  Implementierung der verschiedener Regeleinrichtungen
                   z.B. Proportional-Regler (P-Regler)
    - Signal:      Konstant-, Sprung- oder Sinus-Signal
                   
Durchführen der Simulation in diskreten Zeitschritten gemäss folgendem Muster:
    while heizung.t < simulationsdauer:               # Zeile (1)
        T_current = heizung.step(vorlauf_temperatur)  # Zeile (2)
        vorlauf_temperatur = regler.step(T_current)   # Zeile (3)

    - Zeile 1: Abbruchbedingung (Simulationsdauer)
    - Zeile 2: Simulation des Systems für einen Zeitschritt
               mit Neuberechnung der Raumtemperatur
    - Zeile 3: Neuberechnung der Stellgrösse (Vorlauftemperatur)
               mit den gegebenen Reglerparametern und für die 
               aktuelle Raumtemperatur

@author: birkudo
"""
# %%
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches
from numpy import pi, sin, cos, exp
global heizung, regler

# %% define system
class Raumheizung(object):
    theta_i = 10;      # Innentemperatur zu Beginn
    theta_a = -10;     # aktuelle Aussentemperatur
    Delta_verlust = 10;# Reduktion der Vorlauftemperatur am Heizkörper (weniger warm als Vorlauf)
    dt = 0.25;         # Zeitschritt (Dauer in mins)
    tau_Heiz = 20;     # Zeitkonstante heizen (Raum aufwärmen durch Heizkörper)
    tau_Auskühl = 40;  # Zeitkonstante auskühlen (durch Hauswände)
    tau_Totzeit = 2.5; # Zeitkonstante Totzeit (mins)
    t = 0;             # aktueller Simulations-Zeitpunkt
    
    def __init__(self, params=None):    
        if params is not None:
            for (key, value) in zip(list(params.keys()), list(params.values())):
                available = self.__getattribute__(key)
                self.__setattr__(key, float(value))
        self.T_vorlauf = []
        self.T_innen = []
        self.T_aussen = []
        self.time_points = []
        self.offset_Totzeit = int(self.tau_Totzeit/self.dt)

    def get_params(self):
        return {'theta_i': self.theta_i,
                'theta_a': self.theta_a,
                'tau_Heiz': self.tau_Heiz,
                'tau_Auskühl': self.tau_Auskühl}
    
    def set_params(self, params):
        # get a list of available parameters
        p_available = list(self.get_params().keys())
        for (key, value) in zip(list(params.keys()), list(params.values())):
            if key in p_available:
                self.setValue(key, value)
    
    def setValue(self, key, value):
        try:
            # self.__setattribute__(key, value)
            self.__setattr__(key, float(value))
        except TypeError:
            self.__setattr__(key, value)

    def getValue(self, attr):
        if isinstance(attr, str):
            attr = self.__getattribute__(attr)
        try:
            value = attr(self.t)
        except TypeError:
            value = attr
        return value
        
    def step(self, theta_vorlauf, params=None):
        self.theta_vorlauf = theta_vorlauf
        theta_vorlauf = self.getValue(self.theta_vorlauf)
        if params is not None:
            self.set_params(params)
        T_aussen = self.getValue(self.theta_a)
        if self.t <= self.tau_Totzeit:
            dTdt = 0
            T_innen = self.getValue(self.theta_i)
        else:
            T_innen = self.T_innen[-1]
            T_Heizung = self.T_vorlauf[-1-self.offset_Totzeit] - self.Delta_verlust;
            dTdt = 1/self.tau_Heiz * (T_Heizung-T_innen)-1/self.tau_Auskühl * (T_innen-T_aussen)
        self.T_vorlauf.append(theta_vorlauf)
        T_innen += self.dt*dTdt
        self.T_innen.append(T_innen)
        self.T_aussen.append(T_aussen)
        self.time_points.append(self.t)
        self.t = self.t + self.dt    # progress to next time step
        return T_innen
    
# %% define PID controller
class PID_Regler():
    set_point = 0
    Kp = 0
    Ki = 0
    Kd = 0
    Arbeitspunkt = 0
    dt = 0.25
    t  = 0.0
    
    Integral = []
    Diff = []
    
    def __init__(self, set_point = 0, Kp = 0, Ki = 0, Kd = 0, Arbeitspunkt = 0, dt = 0.25):
        self.set_point = float(set_point)   # Führungsgrösse (aktueller Wert)
        self.Kp = float(Kp)
        self.Ki = float(Ki)
        self.Kd = float(Kd)
        self.Arbeitspunkt = float(Arbeitspunkt)
        self.dt = float(dt)
        
        self.Führungsgrösse = []
        self.e = []
        self.y = []
        
        self.time_points = []
        
    def get_params(self):
        return {'set_point': self.set_point,
                'Kp': self.Kp,
                'Ki': self.Ki,
                'Kd': self.Kd,
                'Arbeitspunkt': self.Arbeitspunkt}
    
    def set_params(self, params):
        # get a list of available parameters
        p_available = list(self.get_params().keys())
        for (key, value) in zip(list(params.keys()), list(params.values())):
            if key in p_available:
                self.setValue(key, value)
    
    def setValue(self, key, value):
        try:
            self.__setattr__(key, float(value))
        except TypeError:
            self.__setattr__(key, value)
    
    def getValue(self, attr):
        if isinstance(attr, str):
            attr = self.__getattribute__(attr)
        try:
            value = attr(self.t)
        except TypeError:
            value = attr
        return value
        
    def step(self, value, params=None):
        if params is not None:
            self.set_params(params)
        set_point = self.getValue(self.set_point)
        e = set_point - value
        # print(f"error = {e}")
        if len(self.e) > 0:
            Integral = self.Integral[-1] + self.dt*e
            Diff = self.e[-1] - e
        else:
            Integral = e
            Diff = 0
        self.Führungsgrösse.append(set_point)
        self.e.append(e)
        self.Integral.append(Integral)
        self.Diff.append(Diff)
        y = ( self.Kp * e  +
              self.Ki * Integral  +
              self.Kd * Diff  +
              self.Arbeitspunkt  )
        self.y.append(y)
        self.time_points.append(self.t)
        self.t = self.t + self.dt    # progress to next time step
        return y

# %% define signals
#    - constSignal = constant signal     (value = baseline)
#    - stepSignal  = step signal         (baseline + (0,1)*amplitude)
#    - sineSignal  = oscillatory signal  (baseline +/- amplitude)
class constSignal(object):
    baseline = 0.0
  
    def __init__(self, params=None):
        if params is None:
            return
        for (key, value) in zip(list(params.keys()), list(params.values())):
            # self.__setattribute__(key, float(value))
            self.__setattr__(key, float(value))
            
    def __call__(self, t):
        value = self.baseline
        return value
        
class stepSignal(object):
    baseline = 0.0
    t_start = 0.0
    amplitude = 1.0
    
    def __init__(self, params=None):
        if params is None:
            return
        for (key, value) in zip(list(params.keys()), list(params.values())):
            # self.__setattribute__(key, float(value))
            self.__setattr__(key, float(value))
            
    def __call__(self, t):
        if t >= self.t_start:
            value = self.baseline + self.amplitude
        else:
            value = self.baseline
        return value
        
class sineSignal(object):
    baseline = 0.0
    amplitude = 1.0
    frequency = 2.0*pi/20
    phase = 0.0
    
    def __init__(self, params=None):
        if params is None:
            return
        for (key, value) in zip(list(params.keys()), list(params.values())):
            # self.__setattribute__(key, float(value))
            self.__setattr__(key, float(value))
            
    def __call__(self, t):
        value = self.amplitude*sin(self.frequency*t+self.phase)+self.baseline
        return value        
        
# %%
def main(simulation_type = ''):
    global heizung, regler
    
    # Schritt 1: Definiere die Regelstrecke (engl. "plant")
    heizung = Raumheizung()   # Raumheizung mit Standardparametern

    # Schritt 2: Definiere die Simulationsparameter
    vorlauf_temperatur = 50    # Startwert für Vorlauftemperatur
    simulationsdauer = 60      # Dauer in Minuten
    clip_simulation = 0        # start index for plot output
    
    # Schritt 3: Definiere den Regler
    # Hinweis: den gewünschten Regler ein-/auskommentieren, 
    # und die gewünschten Parameter einstellen:
    regler  = PID_Regler(Arbeitspunkt = 50)  # ungeregelter Heizkreislauf, gibt immer 50 zurück
    regler  = PID_Regler(set_point = 20, Kp = 7)  # P-Regler
    regler  = PID_Regler(set_point = 20, Kp = 7, Arbeitspunkt = 40)  # P-Regler mit Arbeitspunkt (Vorlauf ca. 40°C)
    regler  = PID_Regler(set_point = 20, Kp = 6.4, Ki = 0.84, Arbeitspunkt = 40)  # P-Regler mit Arbeitspunkt (Vorlauf ca. 40°C)
    

    set_point = regler.__getattribute__('set_point')
    if simulation_type == '':
        pass
    elif simulation_type == 'oszillationsverhalten':
        # simulation type: osczillationsverhalten
        vorlauf_temperatur = sineSignal({'baseline':50,
                        'amplitude':15,
                        'frequency':2*pi/5})
        # regler  = PID_Regler(set_point = 20, Kp = 7, Arbeitspunkt = vorlauf_temperatur)  # P-Regler mit Arbeitspunkt
        
        # constant outside temperature at -10°C
        theta_a = constSignal({'baseline':-10.0})
        
        set_point = 0.0
        # set delay time to zero
        # heizung.__setattr__('tau_Totzeit',0.25)  # totzeit
        # heizung.offset_Totzeit = int(heizung.tau_Totzeit/heizung.dt)
        
        regler = None
    elif simulation_type == 'störverhalten':
        # simulation type: störverhalten

        # define simulation parameters:
        simulationsdauer = 120 # Dauer in Minuten
        clip_simulation = int(np.round(60/heizung.dt))

        vorlauf_temperatur = 45.0
        # regler  = PID_Regler(set_point = 20, Kp = 7, Arbeitspunkt = vorlauf_temperatur)  # P-Regler mit Arbeitspunkt

        # raise outside temperature from -10°C by 20°C (to +10°C) at time 70 mins
        theta_a = stepSignal({'baseline':-10.0,'amplitude':20,'t_start':70})

        # constant target temperature at defined set_point temperature
        w = constSignal({'baseline':set_point})
        
    elif simulation_type == 'führungsverhalten':
        # simulation type: führungsverhalten

        # define simulation parameters:
        simulationsdauer = 120 # Dauer in Minuten
        clip_simulation = int(np.round(60/heizung.dt))

        vorlauf_temperatur = 45.0
        # regler  = PID_Regler(set_point = 20, Kp = 7, Arbeitspunkt = vorlauf_temperatur)  # P-Regler mit Arbeitspunkt
        
        # constant outside temperature at -10°C
        theta_a = constSignal({'baseline':-10.0})
        
        # raise outside temperature from -10°C by 20°C (to +10°C) at time 70 mins
        w = stepSignal({'baseline':set_point,'amplitude':3,'t_start':70})

    elif simulation_type == 'führungsoszillation':
        # simulation type: führungsverhalten
        vorlauf_temperatur = 45.0
        # regler  = PID_Regler(set_point = 20, Kp = 7, Arbeitspunkt = vorlauf_temperatur)  # P-Regler mit Arbeitspunkt
        
        # constant outside temperature at -10°C
        theta_a = constSignal({'baseline':-10.0})
        
        # raise outside temperature from -10°C by 20°C (to +10°C) at time 70 mins
        w = sineSignal({'baseline':set_point,
                        'amplitude':1,
                        'frequency':2*pi/2})
    else:
        raise(Exception(f"ERROR: unknown simulation_type: {simulation_type}"))

    if len(simulation_type)>0:
        heizung.__setattr__('theta_i',set_point)  # start temperature
        heizung.set_params({'theta_a': theta_a})
        if regler is not None:
            regler.set_params({'set_point': w})
            regler.set_params({'Arbeitspunkt': vorlauf_temperatur})
        
    
    # Schritt 4: Simulationsloop
    while heizung.t < simulationsdauer:
        T_current = heizung.step(vorlauf_temperatur)
        if regler is not None:
            vorlauf_temperatur = regler.step(T_current)
        
    # Ausgabe der Ergebnisse    
    outputResults(heizung, regler, clip_simulation, simulation_type)

# %%
def closeResults():
    for simu_type in ['','störverhalten','führungsverhalten']:
        # plt.close('plant:'+simu_type)
        try:
            plt.close('plant:'+simu_type)
        except:
            pass    
        try:
            plt.close('controller:'+simu_type)
        except:
            pass    

def on_keypress(event):
    if event.key == 'escape':
        plt.close(event.canvas.figure)

def dump_to_file(heizung, regler, clip_simulation=0, simulation_type=''):
    time_points = heizung.time_points[clip_simulation:]
    T_innen     = heizung.T_innen[clip_simulation:]
    T_vorlauf   = heizung.T_vorlauf[clip_simulation:]
    T_aussen    = heizung.T_aussen[clip_simulation:]
    
    if regler is None:
        matrix = np.c_[time_points, T_innen, T_vorlauf, T_aussen]
        columns = ','.join(['time_points','T_innen','T_vorlauf','T_aussen'])
    else:
        Regler_e = regler.e[clip_simulation:]
        Regler_y = regler.y[clip_simulation:]
        Regler_w = regler.Führungsgrösse[clip_simulation:]

        matrix = np.c_[time_points, T_innen, T_vorlauf, T_aussen, Regler_e, Regler_y, Regler_w]
        columns = ','.join(['time_points','T_innen','T_vorlauf','T_aussen','Regler_e', 'Regler_y', 'Regler_w'])
    fname = 'simulated_data.csv'
    np.savetxt(fname,matrix,fmt='%9.4f',delimiter=',',header=columns) # ,encoding='utf-8')
    print(f"Simulation gespeichert in '{fname}'")

    
def outputResults(heizung, regler, clip_simulation, simulation_type):
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    
    time_points = heizung.time_points[clip_simulation:]
    fig = plt.figure(num='plant:'+simulation_type)
    plt.figure(fig)
    fig.clf()
    ax = fig.gca()
    ax.plot(time_points, heizung.T_innen[clip_simulation:], label='T_innen')
    ax.plot(time_points, heizung.T_vorlauf[clip_simulation:], label='T_vorlauf', alpha=0.65)
    ax.plot(time_points, heizung.T_aussen[clip_simulation:], label='T_aussen', alpha=0.65)
    ax.set(title='Regelstrecke', xlabel='Zeit (min)')
    val = heizung.T_innen[-1]

    plt.legend()
    
    # beharrungszustand analysieren
    validFluctuations = 0.1
    inRange = np.abs(np.array(heizung.T_innen[clip_simulation:])-val)<=validFluctuations
    inRange = np.argwhere(np.diff(inRange))+1
    if len(inRange)==0:
        inRange = [[0]]
        
    lastIndex = inRange[-1][0]
    x1 = time_points[lastIndex]
    x2 = time_points[-1]
    y1 = val-1; y2 = val+1;
    if x2 >= x1+3:
        plt.fill([x1,x1,x2,x2],[y1,y2,y2,y1],color=colors[0],alpha=0.5,label='Beharrungszustand')

    ax.annotate(f'T_end={val:5.2f}°C', xy=(time_points[-1], val), xycoords='data',
                xytext=(0.85, .75), textcoords='axes fraction',
                va='top', ha='right', color=colors[0],
                arrowprops=dict(facecolor=colors[0], lw=0, shrink=0.02, width=2.0))    

    fig.canvas.draw()
    fig.canvas.mpl_connect('key_press_event', on_keypress)
    
    
    if regler is not None:
        theta_target = regler.Führungsgrösse[-1]
        fig = plt.figure(num='controller:'+simulation_type)
        plt.figure(fig)
        fig.clf()
        ax = fig.gca()
        ax.plot(time_points,regler.e[clip_simulation:],label='Regler e')
        ax.plot(time_points,regler.y[clip_simulation:],':',label='Regler y')
        ax.plot(time_points,regler.Führungsgrösse[clip_simulation:], label='Regler w')
        reglerStr = str([regler.Kp, regler.Ki, regler.Kd])
        ax.set(title=f'Regler {reglerStr}', xlabel='Zeit (min)')
        plt.legend()
        fig.canvas.draw()
        fig.canvas.mpl_connect('key_press_event', on_keypress)
    else:
        theta_target = 20.0
        try:
            plt.close(num='controller:')
        except:
            pass    
    
    val = heizung.T_innen[-1]
    print(f"T_end={val:5.2f}     Temperatur am Ende der Simulation")
    Delta = val-theta_target
    print(f"Δ_end={Delta:5.2f}     Abweichung vom Soll am Ende der Simulation")

# %%
def demo():
    selection = [
        'oszillationsverhalten',
        'führungsverhalten',
        'störverhalten',
        'führungsoszillation',
        '']
    for k, s in enumerate(selection[:-1]):
        print(f"{k}: {s}")
    print("'': default (PID control)")
    print("Please enter the selection and press <ENTER>")
    result = input()
    if result in ['0','1','2','3']:
        simulation_type = selection[int(result)]
    elif result == '':
        simulation_type = ''
    else:
        raise(Exception(f"unknown input: {result}"))
    main(simulation_type)
    
# %%
if __name__=="__main__":
    # Hinweis: ein-/auskommentieren der gewünschten Systemsimulation
    simulation_type = 'führungsoszillation' 
    simulation_type = 'oszillationsverhalten' 
    simulation_type = 'führungsverhalten' 
    simulation_type = 'störverhalten'
    simulation_type = ''  # default simulation type

    # main(simulation_type)
    main()

# %%