# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 10:05:11 2023

https://www.youtube.com/watch?v=quqxyny5kBU

- Generelle Simulation einer Raumheizung
- Modellbildung und Euler-Integration (numerische Lösung der Differentialgleichungen)
    * Aufwärmverhalten des Raums (Heizkörper-Temperatur > Raumtemp.)
    * Zeitverhalten (Heizung im Keller -> Totzeit bis am Heizkörper ankommt)
    * Abkühlverhalten durch Wärmeabgabe (Aussenwand)
- Simulation des Temperaturverhaltens ohne und mit Regeleinrichtung (Regler)
- Implementierung verschiedener Regler-Typen (P-Regler, PI-Regler, etc. mit/ohne Arbeitspunkt)

Zwei Klassen:
    - Raumheizung: numerische Simulation des Systems
    - PID_Regler:  Implementierung der verschiedener Regeleinrichtungen
                   z.B. Proportional-Regler (P-Regler)
                   
Durchführen der Simulation in diskreten Zeitschritten gemäss folgendem Muster:
    while heizung.t < simulationsdauer:               # Zeile (1)
        T_current = heizung.step(vorlauf_temperatur)  # Zeile (2)
        vorlauf_temperatur = regler.step(T_current)   # Zeile (3)

    - Zeile 1: Abbruchbedingung (Simulationsdauer)
    - Zeile 2: Simulation des Systems für einen Zeitschritt
               mit Neuberechnung der Raumtemperatur
    - Zeile 3: Neuberechnung der Stellgrösse (Vorlauftemperatur)
               mit den gegebenen Reglerparametern und für die 
               aktuelle Raumtemperatur

@author: birkudo
"""
# %%
import numpy as np
import matplotlib.pyplot as plt

global heizung, regler

# %%
class Raumheizung(object):
    theta_i = 10;      # Innentemperatur zu Beginn
    theta_a = -10;     # aktuelle Aussentemperatur
    Delta_verlust = 10;# Reduktion der Vorlauftemperatur am Heizkörper (weniger warm als Vorlauf)
    dt = 0.25;         # Zeitschritt (Dauer in mins)
    tau_Heiz = 20;     # Zeitkonstante heizen (Raum aufwärmen durch Heizkörper)
    tau_Auskühl = 40;  # Zeitkonstante auskühlen (durch Hauswände)
    tau_Totzeit = 2.5; # Zeitkonstante Totzeit (mins)
    t = 0;             # aktueller Simulations-Zeitpunkt
    
    def __init__(self, params=None):    
        if params is not None:
            self.set_params(params)
        self.T_vorlauf = []     # Liste der Vorlauftemperatur zu jedem Zeitschritt
        self.T_innen = []       # Liste der Innentemperatur zu jedem Zeitschritt
        self.T_aussen = []      # Liste der Aussentemperatur zu jedem Zeitschritt
        self.time_points = []   # Liste der Zeitschritte
        self.offset_Totzeit = int(self.tau_Totzeit/self.dt)

    def step(self, theta_vorlauf, params=None):
        if params is not None:
            self.set_params(params)
        T_aussen = self.theta_a
        if self.t <= self.tau_Totzeit:
            dTdt = 0
            T_innen = self.theta_i
        else:
            T_innen = self.T_innen[-1]
            T_Heizung = self.T_vorlauf[-1-self.offset_Totzeit] - self.Delta_verlust;
            dTdt = 1/self.tau_Heiz * (T_Heizung-T_innen)-1/self.tau_Auskühl * (T_innen-T_aussen)
        self.T_vorlauf.append(theta_vorlauf)
        T_innen += self.dt*dTdt
        self.T_innen.append(T_innen)
        self.T_aussen.append(T_aussen)
        self.time_points.append(self.t)
        self.t = self.t + self.dt    # progress to next time step
        return T_innen
    
# %%
class PID_Regler():
    Führungsgrösse = 0
    Kp = 0
    Ki = 0
    Kd = 0
    Arbeitspunkt = 0
    dt = 0.25
    
    Integral = []
    Diff = []
    
    def __init__(self, set_point = 0, Kp = 0, Ki = 0, Kd = 0, Arbeitspunkt = 0, dt = 0.25):
        self.set_point = float(set_point)   # Führungsgrösse (aktueller Wert)
        self.Kp = float(Kp)
        self.Ki = float(Ki)
        self.Kd = float(Kd)
        self.Arbeitspunkt = float(Arbeitspunkt)
        self.dt = float(dt)
        
        self.Führungsgrösse = []
        self.e = []
        self.y = []
        
    def step(self, value, params=None):
        if params is not None:
            self.set_params(params)
        e = self.set_point - value
        # print(f"error = {e}")
        if len(self.e) > 0:
            Integral = self.Integral[-1] + self.dt*e
            Diff = self.e[-1] - e
        else:
            Integral = e
            Diff = 0
        self.Führungsgrösse.append(self.set_point)
        self.e.append(e)
        self.Integral.append(Integral)
        self.Diff.append(Diff)
        y = ( self.Kp * e  +
              self.Ki * Integral  +
              self.Kd * Diff  +
              self.Arbeitspunkt  )
        self.y.append(y)
        return y

# %%
def main():
    global heizung, regler
    
    # Schritt 1: Definiere die Regelstrecke (engl. "plant")
    heizung = Raumheizung()   # Raumheizung mit Standardparametern

    # Schritt 2: Definiere die Simulationsparameter
    vorlauf_temperatur = 50    # Startwert für Vorlauftemperatur
    theta_target = 20;         # Zieltemperatur
    simulationsdauer = 60      # Dauer in Minuten

    # Schritt 3: Definiere den Regler
    # den gewünschten Regler ein-/auskommentieren, und die
    # gewünschten Parameter einstellen:    
    regler  = PID_Regler(Arbeitspunkt = 50)  # ungeregelter Heizkreislauf, gibt immer 50 zurück
    regler  = PID_Regler(set_point = theta_target, Kp = 7)  # P-Regler
    regler  = PID_Regler(set_point = theta_target, Kp = 7, Arbeitspunkt = 40)  # P-Regler mit Arbeitspunkt (Vorlauf ca. 40°C)
    regler  = PID_Regler(set_point = theta_target, Kp = 6.4, Ki = 0.84, Arbeitspunkt = 40)  # P-Regler mit Arbeitspunkt (Vorlauf ca. 40°C)
    regler  = None
        

    # Schritt 4: Simulationsloop
    while heizung.t < simulationsdauer:
        T_current = heizung.step(vorlauf_temperatur)
        if regler is not None:
            vorlauf_temperatur = regler.step(T_current)


    # Ausgabe der Ergebnisse    
    time_points = heizung.time_points
    fig = plt.figure(num='plant:')
    fig.clf()
    ax = fig.gca()
    ax.plot(time_points, heizung.T_innen, label='T_innen')
    ax.plot(time_points, heizung.T_vorlauf, label='T_vorlauf')
    ax.plot(time_points, heizung.T_aussen, label='T_aussen')
    ax.set(title='Regelstrecke', xlabel='Zeit (min)')
    plt.legend()
    
    if regler is not None:
        fig = plt.figure(num='controller:')
        fig.clf()
        ax = fig.gca()
        ax.plot(time_points,regler.e,label='Regler e')
        ax.plot(time_points,regler.y,label='Regler y')
        ax.plot(time_points,regler.Führungsgrösse,label='Regler w')
        ax.set(title='Regler', xlabel='Zeit (min)')
        plt.legend()
    else:
        try:
            plt.close('controller:')
        except:
            pass    
    val = heizung.T_innen[-1]
    print(f"T_end={val:5.2f}     Temperatur am Ende der Simulation")
    Delta = val-theta_target
    print(f"Δ_end={Delta:5.2f}     Abweichung vom Soll am Ende der Simulation")

# %%
if __name__=="__main__":
    main()

# %%